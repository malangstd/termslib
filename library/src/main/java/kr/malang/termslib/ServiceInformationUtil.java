package kr.malang.termslib;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * Created by kyungoh on 2017. 10. 11..
 */

public class ServiceInformationUtil {
    public static final String KEY_TERMS_OF_USE_AGREE_DATE = "kr.malang.termslib.KEY_TERMS_OF_USE_AGREE_DATE";
    public static final String KEY_PRIVACY_POLICY_AGREE_DATE = "kr.malang.termslib.KEY_PRIVACY_POLICY_AGREE_DATE";
    public static final String KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE = "kr.malang.termslib.KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE";


    public static boolean isTermsAgreeAll(Context context, boolean termsOfUse, boolean privacyPolicy, boolean termsOfUseOfLocation) {

        if (termsOfUse) {
            if (TextUtils.isEmpty(getProperty(context, ServiceInformationUtil.KEY_TERMS_OF_USE_AGREE_DATE, null))) {
                return false;
            }
        }

        if (privacyPolicy) {
            if (TextUtils.isEmpty(getProperty(context, ServiceInformationUtil.KEY_PRIVACY_POLICY_AGREE_DATE, null))) {
                return false;
            }
        }


        if(termsOfUseOfLocation) {
            if (TextUtils.isEmpty(getProperty(context, ServiceInformationUtil.KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE, null))) {
                return false;
            }
        }


        return true;
    }

    public static void setTermsAgreeCancel(Context context) {
        setProperty(context, ServiceInformationUtil.KEY_TERMS_OF_USE_AGREE_DATE, null);
        setProperty(context, ServiceInformationUtil.KEY_PRIVACY_POLICY_AGREE_DATE, null);
        setProperty(context, ServiceInformationUtil.KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE, null);
    }

    public static String getProperty(Context context, String key, String defValue) {
        try {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            return pref.getString(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void setProperty(Context context, String key, String value) {
        try {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor edit = pref.edit();
            edit.putString(key, value);
            edit.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
