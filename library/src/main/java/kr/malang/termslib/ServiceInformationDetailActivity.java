package kr.malang.termslib;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by kyungoh on 2017. 9. 28..
 */

public class ServiceInformationDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private WebView mWebView;
    private TextView mAgreeTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.terms_lib_activity_service_information_detail);

        findViewById(R.id.terms_lib_btn_arrow_back).setOnClickListener(this);
        findViewById(R.id.terms_lib_title_layout).setBackgroundColor(getIntent().getIntExtra("title_color", ContextCompat.getColor(this, R.color.terms_lib_color_ff45A0D9)));

        mWebView = (WebView) findViewById(R.id.terms_lib_content_webview);
        mWebView.setWebViewClient(new MyWebClient());
        mAgreeTextView = (TextView) findViewById(R.id.terms_lib_agree_textview);

        String type = getIntent().getStringExtra("TYPE");
        String url = getIntent().getStringExtra("URL");

        try {
            switch (type) {
                case "button_terms_of_use":
                    ((TextView) findViewById(R.id.terms_lib_app_bar_title)).setText(R.string.terms_lib_terms_of_use);

                    if (TextUtils.isEmpty(ServiceInformationUtil.getProperty(this, ServiceInformationUtil.KEY_TERMS_OF_USE_AGREE_DATE, null))) {
                        mAgreeTextView.setVisibility(View.VISIBLE);
                        mAgreeTextView.setOnClickListener(this);
                    } else {
                        mAgreeTextView.setVisibility(View.GONE);
                    }
                    break;

                case "button_privacy_policy":
                    ((TextView) findViewById(R.id.terms_lib_app_bar_title)).setText(R.string.terms_lib_privacy_policy);

                    if (TextUtils.isEmpty(ServiceInformationUtil.getProperty(this, ServiceInformationUtil.KEY_PRIVACY_POLICY_AGREE_DATE, null))) {
                        mAgreeTextView.setVisibility(View.VISIBLE);
                        mAgreeTextView.setOnClickListener(this);
                    } else {
                        mAgreeTextView.setVisibility(View.GONE);
                    }
                    break;

                case "button_terms_of_use_of_location":
                    ((TextView) findViewById(R.id.terms_lib_app_bar_title)).setText(R.string.terms_lib_terms_of_use_of_location_based_service);

                    if (TextUtils.isEmpty(ServiceInformationUtil.getProperty(this, ServiceInformationUtil.KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE, null))) {
                        mAgreeTextView.setVisibility(View.VISIBLE);
                        mAgreeTextView.setOnClickListener(this);
                    } else {
                        mAgreeTextView.setVisibility(View.GONE);
                    }
                    break;

                default:
                    finish();
                    break;
            }

            mWebView.loadUrl(url);
        } catch (Exception e) {
            finish();
        }
    }

    @Override
    public void onClick(View v) {

        if (mAgreeTextView.equals(v)) {
            String type = getIntent().getStringExtra("TYPE");

            switch (type) {
                case "button_terms_of_use":
                    ServiceInformationUtil.setProperty(this, ServiceInformationUtil.KEY_TERMS_OF_USE_AGREE_DATE, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                    break;

                case "button_privacy_policy":
                    ServiceInformationUtil.setProperty(this, ServiceInformationUtil.KEY_PRIVACY_POLICY_AGREE_DATE, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                    break;

                case "button_terms_of_use_of_location":
                    ServiceInformationUtil.setProperty(this, ServiceInformationUtil.KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                    break;
            }
        }

        finish();
    }

    class MyWebClient extends WebViewClient {

        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }
}
