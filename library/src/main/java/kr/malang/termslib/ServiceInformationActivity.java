package kr.malang.termslib;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by kyungoh on 2017. 9. 28..
 */

public class ServiceInformationActivity extends AppCompatActivity implements View.OnClickListener {
    private View mTermsOfUse;
    private View mPrivacyPolicy;
    private View mTermsOfUseOfLocation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.terms_lib_activity_service_information);

        findViewById(R.id.terms_lib_btn_arrow_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAgreeResult();
            }
        });

        findViewById(R.id.terms_lib_title_layout).setBackgroundColor(getIntent().getIntExtra("title_color", ContextCompat.getColor(this, R.color.terms_lib_color_ff45A0D9)));

        mTermsOfUse = findViewById(R.id.terms_lib_button_terms_of_use);
        mPrivacyPolicy = findViewById(R.id.terms_lib_button_privacy_policy);
        mTermsOfUseOfLocation = findViewById(R.id.terms_lib_button_terms_of_use_of_location);

        if (TextUtils.isEmpty(getIntent().getStringExtra("button_terms_of_use_url"))) {
            ((LinearLayout) mTermsOfUse.getParent()).setVisibility(View.GONE);
        } else {
            ((LinearLayout) mTermsOfUse.getParent()).setVisibility(View.VISIBLE);
            mTermsOfUse.setTag(getIntent().getStringExtra("button_terms_of_use_url"));
            mTermsOfUse.setOnClickListener(this);
        }

        if (TextUtils.isEmpty(getIntent().getStringExtra("button_privacy_policy_url"))) {
            ((LinearLayout) mPrivacyPolicy.getParent()).setVisibility(View.GONE);
        } else {
            ((LinearLayout) mPrivacyPolicy.getParent()).setVisibility(View.VISIBLE);
            mPrivacyPolicy.setTag(getIntent().getStringExtra("button_privacy_policy_url"));
            mPrivacyPolicy.setOnClickListener(this);
        }

        if (TextUtils.isEmpty(getIntent().getStringExtra("button_terms_of_use_of_location_url"))) {
            ((LinearLayout) mTermsOfUseOfLocation.getParent()).setVisibility(View.GONE);
        } else {
            ((LinearLayout) mTermsOfUseOfLocation.getParent()).setVisibility(View.VISIBLE);
            mTermsOfUseOfLocation.setTag(getIntent().getStringExtra("button_terms_of_use_of_location_url"));
            mTermsOfUseOfLocation.setOnClickListener(this);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (TextUtils.isEmpty(ServiceInformationUtil.getProperty(this, ServiceInformationUtil.KEY_TERMS_OF_USE_AGREE_DATE, null))) {
            mTermsOfUse.setActivated(false);
        } else {
            mTermsOfUse.setActivated(true);
        }

        if (TextUtils.isEmpty(ServiceInformationUtil.getProperty(this, ServiceInformationUtil.KEY_PRIVACY_POLICY_AGREE_DATE, null))) {
            mPrivacyPolicy.setActivated(false);
        } else {
            mPrivacyPolicy.setActivated(true);
        }

        if (TextUtils.isEmpty(ServiceInformationUtil.getProperty(this, ServiceInformationUtil.KEY_TERMS_OF_USE_OF_LOCATION_AGREE_DATE, null))) {
            mTermsOfUseOfLocation.setActivated(false);
        } else {
            mTermsOfUseOfLocation.setActivated(true);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ServiceInformationDetailActivity.class);

        intent.putExtra("title_color", getIntent().getIntExtra("title_color", ContextCompat.getColor(this, R.color.terms_lib_color_ff45A0D9)));

        if (mTermsOfUse == v) {
            intent.putExtra("TYPE", "button_terms_of_use");
            intent.putExtra("URL", (String) v.getTag());
            startActivity(intent);
        } else if (mPrivacyPolicy == v) {
            intent.putExtra("TYPE", "button_privacy_policy");
            intent.putExtra("URL", (String) v.getTag());
            startActivity(intent);
        } else if (mTermsOfUseOfLocation == v) {
            intent.putExtra("TYPE", "button_terms_of_use_of_location");
            intent.putExtra("URL", (String) v.getTag());
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        setAgreeResult();
    }

    private void setAgreeResult() {
        if (!TextUtils.isEmpty(getIntent().getStringExtra("button_terms_of_use_url"))) {
            if (!mTermsOfUse.isActivated()) {
                setResult(RESULT_CANCELED);
                finish();
                return;
            }
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra("button_privacy_policy_url"))) {
            if (!mPrivacyPolicy.isActivated()) {
                setResult(RESULT_CANCELED);
                finish();
                return;
            }
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra("button_terms_of_use_of_location_url"))) {
            if (!mTermsOfUseOfLocation.isActivated()) {
                setResult(RESULT_CANCELED);
                finish();
                return;
            }
        }

        setResult(RESULT_OK);
        finish();
    }
}
